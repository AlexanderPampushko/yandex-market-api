## Описание проекта
 
Java-клиент для работы с API Yandex Market 

и Places HTTP API (API Поиска по организациям Яндекса). 

Документация здесь: 
[Maps API - Geosearch](https://tech.yandex.ru/maps/doc/geosearch/concepts/response_structure_business-docpage/)
и здесь: [Yandex Market API - Content Data](https://tech.yandex.ru/market/content-data/)
 
## Мотивация
 
Не смог найти удобного java-клиента для работы с REST API Yandex
 
## Настройки
Поместите в директорию resources
файл config.properties
со значениями
- yandex.market.api.key=[your api key]
- yandex.market.baseUrl=https://api.content.market.yandex.ru/v1/
 
## Функциональность
### Получение отзывов о магазине по его id
    client.getShopOpinion(shopId)
 
## Contributors
 
Alexander Pampushko
 
## License
 
BSD license
