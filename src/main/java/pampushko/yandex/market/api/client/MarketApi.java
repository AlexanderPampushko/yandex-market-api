package pampushko.yandex.market.api.client;

import pampushko.yandex.market.api.models.market.ShopOpinionsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Интерфейс, описывающий функции Yandex Market API
 * <br />
 * Описание используется Retrofit-клиентом
 * <br />
 */
public interface MarketApi
{
	/**
	 * @return Возвращает объект содержащий список отзывов и доп. информацию - {@code ShopOpinionsResponse}
	 * <br />
	 */
	@GET("shop/{shop_id}/opinion.json")
	Call<ShopOpinionsResponse> getShopOpinion(final @Path("shop_id") String shopId);
}
