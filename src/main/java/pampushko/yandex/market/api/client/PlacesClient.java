package pampushko.yandex.market.api.client;

import lombok.extern.slf4j.Slf4j;

/**
 * Клиент для API Поиска по организациям позволяет находить организации по названию,
 * <br />
 * адресу, номеру телефона, виду услуг.
 * <br />
 * По каждому найденному объекту вы получите подробную информацию
 * <br />
 * — например, контакты и режим работы.
 * <br />
 */
@Slf4j
public class PlacesClient
{

}
