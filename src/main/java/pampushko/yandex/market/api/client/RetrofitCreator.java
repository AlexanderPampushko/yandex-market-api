package pampushko.yandex.market.api.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

/**
 * Класс содержащий методы для создания Retrofit экземпляров
 * <br />
 * со всеми необходимыми нам настройками
 * <br />
 */
@Slf4j
public class RetrofitCreator
{
	/**
	 * Создаем Retrofit экземпляр для создания клиента для работы
	 * <br />
	 * с Yandex Market API
	 * <br />
	 *
	 * @param marketApiClient клиент для работы с Yandex Market (из клиента берем credetials, baseUrl)
	 * @return экземпляр Retrofit
	 */
	Retrofit getRetrofitForMarketApi(MarketClient marketApiClient)
	{
		final String apiKey = marketApiClient.apiKey;
		
		//создаем gson-билдер
		Gson gson = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
				.disableHtmlEscaping()
				.create();
		
		//создаем интерсептор для логирования
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		
		//создаем http-клиента OkHttp и добавляем в него интерсептор (чтобы добавить нужные нам заголовки к
		// каждому из посылаемых нами запросов)
		OkHttpClient httpClient = new OkHttpClient.Builder()
				.addInterceptor(new Interceptor()
				{
					@Override
					public Response intercept(Chain chain) throws IOException
					{
						Request request = chain.request().newBuilder()
								.addHeader("Accept", "application/json")
								.addHeader("Authorization", apiKey)
								.addHeader("User-Agent", "curl/7.47.0")
								.build();
						return chain.proceed(request);
					}
				})
				.addInterceptor(interceptor)
				.build();
		//создаем экземпляр Ретрофита - добавляем к ретрофиту созданный нами ранее Http-клиент
		Retrofit retrofit = new Retrofit.Builder()
				
				.baseUrl(marketApiClient.baseUrl)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.client(httpClient)
				.build();
		
		return retrofit;
	}
}
