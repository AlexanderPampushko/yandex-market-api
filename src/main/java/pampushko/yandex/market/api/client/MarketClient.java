package pampushko.yandex.market.api.client;

import lombok.extern.slf4j.Slf4j;
import pampushko.yandex.market.api.models.market.ShopOpinionsResponse;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;

/**
 * Класс клиента для Yandex Market API
 * <br />
 */
@Slf4j
public class MarketClient
{
	/**
	 * Ключ для взаимодействия с Yandex Market API
	 * <br />
	 */
	String apiKey;
	
	/**
	 * базовый URL для Yandex Market API
	 * <br />
	 */
	String baseUrl;
	
	/**
	 * Ссылка на экземпляр интерфейса MarketApi
	 * <br />
	 */
	private MarketApi marketApi;
	
	/**
	 * Приватный конструктор,
	 * <br />
	 * т.к. мы будем создавать экземпляр клиента через Builder
	 * <br />
	 */
	private MarketClient()
	{
	
	}
	
	///////////////////////////////////////////////////
	public class Builder
	{
		private Builder()
		{
		
		}
		
		public Builder apiKey(String apiKey)
		{
			MarketClient.this.apiKey = apiKey;
			return this;
		}
		
		public Builder baseUrl(String baseUrl)
		{
			MarketClient.this.baseUrl = baseUrl;
			return this;
		}
		
		/**
		 * настраиваем REST-адаптер, который будет использоваться для работы с Yandex Market API
		 * <br />
		 * И создаем Retrofit-клиента, согласно описанию API в интерфейсе {@link pampushko.yandex.market.api.client.MarketApi}
		 * <br />
		 *
		 * @return
		 */
		public MarketClient build()
		{
			Retrofit retrofit = new RetrofitCreator().getRetrofitForMarketApi(MarketClient.this);
			MarketClient.this.marketApi = retrofit.create(MarketApi.class);
			return MarketClient.this;
		}
	}
	///////////////////////////////////////////////////
	
	/**
	 * создаём новый builder для создания {@link pampushko.yandex.market.api.client.MarketClient}
	 *
	 * @return экземпляр Builder-ра
	 */
	public static Builder newBuilder()
	{
		return new MarketClient().new Builder();
	}
	
	
	//////////////////////////////////////////////////////////////
	/*Методы клиента */
	//////////////////////////////////////////////////////////////
	
	/**
	 * @return Возвращает объект содержащий список отзывов и доп. информацию - {@code ShopOpinionsResponse}
	 * <br />
	 */
	public ShopOpinionsResponse getShopOpinion(final String shopId)
	{
		try
		{
			Call<ShopOpinionsResponse> shopOpinionsCall = marketApi.getShopOpinion(shopId);
			Response<ShopOpinionsResponse> response = shopOpinionsCall.execute();
			ShopOpinionsResponse body = response.body();
			return body;
		}
		catch (IOException ex)
		{
			log.error(ex.getMessage());
		}
		return null;
	}
}
