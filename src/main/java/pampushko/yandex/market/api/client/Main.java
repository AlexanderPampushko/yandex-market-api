package pampushko.yandex.market.api.client;

import lombok.extern.slf4j.Slf4j;
import pampushko.yandex.market.api.models.market.Opinion;
import pampushko.yandex.market.api.models.market.ShopOpinions;
import pampushko.yandex.market.api.models.market.ShopOpinionsResponse;
import pampushko.yandex.market.api.settings.SettingsManager;

import java.util.List;
import java.util.Properties;

/**
 * тестируем получение отзывов по магазину с данным id
 * <br />
 */
@Slf4j
public class Main
{
	public static void main(String[] args)
	{
		//читаем настройки приложения
		Properties settings = SettingsManager.getValues(); //todo сделай нормально пожалуйста
		
		//вызываем билдер и создаем клиент
		MarketClient marketClient = MarketClient.newBuilder()
				.baseUrl(settings.getProperty("yandex.market.baseUrl"))
				.apiKey(settings.getProperty("yandex.market.api.key"))
				.build();
		
		ShopOpinionsResponse shopOpinionsResponse = marketClient.getShopOpinion("1163");
		if (shopOpinionsResponse != null)
		{
			ShopOpinions shopOpinions = shopOpinionsResponse.getShopOpinions();
			List<Opinion> opinions = shopOpinions.getOpinion();
			for (Opinion opinion : opinions)
			{
				System.out.println("\n=====================================================\n");
				System.out.println(opinion.getText());
			}
		}
		else
		{
			System.out.println("Shop not found");
		}
	}
}
