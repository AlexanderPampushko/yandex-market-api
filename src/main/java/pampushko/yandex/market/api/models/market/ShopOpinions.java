package pampushko.yandex.market.api.models.market;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import pampushko.yandex.market.api.models.BaseModel;

import java.util.List;

/**
 *
 */
@Slf4j
@Data
public class ShopOpinions extends BaseModel
{
	@SerializedName("opinion")
	private List<Opinion> opinion = null;
	
	@SerializedName("total")
	private Long total;
	
	@SerializedName("page")
	private Long page;
	
	@SerializedName("count")
	private Long count;
}
