package pampushko.yandex.market.api.models.market;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 *
 */
@Data
@Slf4j
public class AuthorInfo
{
	@SerializedName("avatarUrl")
	@Expose
	private String avatarUrl;
	
	@SerializedName("grades")
	@Expose
	private Long grades;
	
	@SerializedName("uid")
	@Expose
	private Long uid;
}
