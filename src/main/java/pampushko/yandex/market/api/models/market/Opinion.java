package pampushko.yandex.market.api.models.market;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 *
 */
@Data
@Slf4j
public class Opinion
{
	@SerializedName("id")
	@Expose
	private Long id;
	
	@SerializedName("date")
	@Expose
	private Long date;
	
	@SerializedName("grade")
	@Expose
	private Long grade;
	
	@SerializedName("text")
	@Expose
	private String text;
	
	@SerializedName("agree")
	@Expose
	private Long agree;
	
	@SerializedName("reject")
	@Expose
	private Long reject;
	
	@SerializedName("visibility")
	@Expose
	private String visibility;
	
	@SerializedName("author")
	@Expose
	private String author;
	
	@SerializedName("authorInfo")
	@Expose
	private AuthorInfo authorInfo;
	
	@SerializedName("comments")
	@Expose
	private List<Object> comments = null;
	
	@SerializedName("shop")
	@Expose
	private Shop shop;
	
	@SerializedName("problem")
	@Expose
	private String problem;
	
	@SerializedName("shopId")
	@Expose
	private Long shopId;
	
	@SerializedName("anonymous")
	@Expose
	private Boolean anonymous;
	
	@SerializedName("convenienceGrade")
	@Expose
	private Long convenienceGrade;
	
	@SerializedName("deliveryGrade")
	@Expose
	private Long deliveryGrade;
}
