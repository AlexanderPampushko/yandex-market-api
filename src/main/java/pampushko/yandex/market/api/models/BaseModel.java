package pampushko.yandex.market.api.models;

import com.google.gson.GsonBuilder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;


/**
 * Базовый класс для всех моделей, которые мы будем использовать
 * <br />
 */
@Slf4j
@Data
public class BaseModel
{
	@Override
	public String toString()
	{
		return new GsonBuilder().disableHtmlEscaping().create().toJson(this);
	}
}
