package pampushko.yandex.market.api.models.market;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 *
 */
@Data
@Slf4j
public class ShopOpinionsResponse
{
	@SerializedName("shopOpinions")
	private ShopOpinions shopOpinions;
}
